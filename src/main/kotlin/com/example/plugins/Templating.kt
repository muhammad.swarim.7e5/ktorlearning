package com.example.plugins

import io.ktor.server.application.*
import io.ktor.server.thymeleaf.Thymeleaf
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver

fun Application.configureTemplating() {
    install(Thymeleaf) {
        setTemplateResolver(ClassLoaderTemplateResolver().apply {
            prefix = "templates/thymeleaf/"
            suffix = ".html"
            characterEncoding = "utf-8"
        })
    }
}

data class ThymeleafUser(val id: Int, val name: String)
data class Task(val name: String, val description: String, val priority: Priority)
enum class Priority {
    Low, Medium, High, Vital
}
