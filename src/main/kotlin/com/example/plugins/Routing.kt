package com.example.plugins

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.server.thymeleaf.ThymeleafContent

fun Application.configureRouting() {
    routing {
        // Define the route for the root path
        get("/") {
            call.respondText(
                contentType = ContentType.Text.Html,
                text = """
                <h1>Welcome to the Ktor Web Application</h1>
                <p>Navigate to <a href="/tasks">/tasks</a> to see the tasks.</p>
                <p>Navigate to <a href="/html-thymeleaf">/html-thymeleaf</a> to see Thymeleaf example.</p>
                """.trimIndent()
            )
        }

        // Define the route for /tasks with Thymeleaf templating
        get("/tasks") {
            val tasks = listOf(
                Task("cleaning", "cleaning the house", Priority.Low),
                Task("gardening", "Mow the lawn", Priority.Medium),
                Task("shopping", "Buy some groceries", Priority.High),
                Task("painting", "Paint the fence", Priority.Vital)
            )
            call.respond(ThymeleafContent("all-tasks", mapOf("tasks" to tasks)))
        }

        // Define the route for /html-thymeleaf with Thymeleaf templating
        get("/html-thymeleaf") {
            call.respond(ThymeleafContent("index", mapOf("user" to ThymeleafUser(1, "user1"))))
        }
    }
}


//Primera proba de push a gitlab
